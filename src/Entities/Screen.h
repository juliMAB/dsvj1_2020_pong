#ifndef SCREEN_H
#define SCREEN_H

const int screenWidth = 800; //ANCHO DE LA PANTALLA.
const int screenHeight = 450; //ALTO DE LA PANTALLA.

const int middlescreenWidth = screenWidth / 2; //MITAD DEL ANCHO DE LA PANTALLA.
const int middlescreenHeight = screenHeight / 2; //MITAD DEL ALTO DE LA PANTALLA.

//PARA LOGRAR UNA MEJOR DISTRIBUCION DE EL VISUAL.

const int Line5Screen = screenHeight / 5; // dividir la pantalla en 5 lineas.
const int Line10Screen = screenHeight / 10; // dividir la pantalla en 10 lineas.

#endif