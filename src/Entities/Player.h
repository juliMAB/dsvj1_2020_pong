#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

#include "Screen.h"

struct Shield
{
	Rectangle shield = { 0, 0, screenWidth / 90,screenHeight }; // DIMENSIONES y POSICION DEL ESCUDO.
	bool shieldBool = false; // SI ESTA ACTIVO EXISTE.
};

struct PLAYER
{
	Rectangle pos = { 10, 0, screenWidth / 70,screenHeight / 7 }; //x,y,width,hight. //POSICION, Y ESCALA DEL RECTANGULO DEL PADDLE.

	float speed = 4; //movement. //VELOCIDAD DE DESPLAZAMIENTO.

	int puntaje = 0; //CANTIDAD DE PUNTOS OBTENIDOS POR EL PLAYER.

	Color color = RED; //COLOR DE EL PADDLE.

	short Ncolor = 0; // VARIABLE PARA PERMITIR EL INTERCAMBIO DE COLOR.

	Shield shield; // VARIABLE ESCUDO DEL PADDLE.

	char keyUp='A'; // VARIABLE PARA DESPLAZARCE HACIA ARRIBA.

	char keyDown = 'Z'; //VARIABLE PARA DESPLAZARCE HACIA ABAJO.

};

#endif