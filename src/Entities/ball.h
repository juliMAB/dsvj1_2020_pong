#ifndef BALL_H
#define BALL_H

#include "raylib.h"

#include "ENUMS/shock.h"
#include "ENUMS/direction.h"
#include "ENUMS/Winers.h"

struct BALL
{
	Vector2 pos = { 0,-100 };//POSICION DE LA BOLA. // DEBERIA NO SER DECLARADO ACA.

	DIRECTION direction = DIRECTION::DOWNLEFT; //DIRECCION PARA DONDE SALE LA BOLA DISPARADA.

	bool coli = false; //Si la BOLA ESTA COLISIONANDO.

	float speed = 0; //VELOCIDAD DE DESPLAZAMIENTO DE LA BOLA.

	SHOCK Shock = SHOCK::DOWN; //DE DONDE RECIBIO INPACTO POR ULTIMA VEZ LA BOLA.

	float radio = 10; //EL RADIO DE LA BOLA.

	Color color = VIOLET; //COLOR DE LA BOLA.

	float MoreSpeed = (float)0.09; //AUMENTO DE VELOCIDAD CADA EVENTO.

	bool existo = false; //PARA EL CASO DE LA BOLA 2, NO HACER UPDATE.

	PLAYERS lastPlayerTouch = PLAYERS::NONE; //PARA GUARDAR EL JUGADOR QUE ULTIMO TOCO LA PELOTA PARA ASIGNARLE EL PODER.
};

#endif