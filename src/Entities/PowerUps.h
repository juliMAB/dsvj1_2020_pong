#ifndef POWERUPS_H
#define POWERUPS_H

#include "raylib.h"

#include <time.h>

#include "Screen.h"

#include "ENUMS/Powers.h"
#include "ENUMS/Winers.h"

namespace powers
{
	struct OBSTACLES
	{
		Rectangle rec1 = { middlescreenWidth - (middlescreenWidth / 3),screenHeight / 3,middlescreenWidth / 70,screenHeight / 7 };
		Rectangle rec2 = { middlescreenWidth - ((middlescreenWidth/3)*2),screenHeight / 2,middlescreenWidth / 70,screenHeight / 7 };
	};

	namespace up
	{

	}
	namespace time
	{
		struct POWERxTIME
		{
			int duration = 0; //tiempo que dura.
			OBSTACLES obst; //LOS RECTANGULOS.
			bool IDirect = false; //BOOL DE INVERTIRDIRECCION.
			bool obstB = false; //BOOL DE OBSTACULOS.
			bool IControls = false; //BOOL DE INVERTIRCONTROLES.
		};
		
	}
	namespace entity//posicion, radio, color, tiempo y colision de el power up y existencia.
	{
		struct POWER
		{
			Vector2 pos = { 0,0 }; //POSICION DEL POWER UP.
			int radio = 100; //RADIO DEL POWERUP. (SI ES UNA BOLITA).
			Color color = { 189,183,135,100 }; //COLOR DEL POWERUP.
			float time = 0; //el tiempo que dura en el espacio.
			bool exist = false; //si esta presente en el espacio.
			POWER_UPS power_actual = POWER_UPS::none; //para hacer un rand y asignarlo dependiendo de que sea.
		};
		
	}
}

#endif