#ifndef WINERS_H
#define WINERS_H
enum class WINER
{
	ONE,
	TWO,
	NONE

};
enum class BOTONS
{
	NONE,
	ONE,
	TWO,
	THREE,
	FOUR

};

enum class PLAYERS
{
	ONE,
	TWO,
	NONE
};
#endif