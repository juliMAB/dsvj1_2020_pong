#ifndef MENUS_H
#define MENUS_H
enum class MENU
{
	INTRO,
	GAME,
	OPTIONS,
	GENERAL,
	PAUSE,
	EXIT,
	CREDITS
};
enum class GAME
{
	CONFIGPADS,
	GAME,
	RESET,
	END,
	PAUSE
};

#endif