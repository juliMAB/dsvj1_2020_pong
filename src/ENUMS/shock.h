#ifndef SHOCK_H
#define SHOCK_H
enum class SHOCK
{
	DOWN,
	RIGHT,
	UP,
	LEFT
};
#endif
