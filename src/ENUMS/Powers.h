#ifndef POWERS_H
#define POWERS_H

enum class POWER_UPS
{
	shield,
	bigpad,
	morespeed,
	twoball,
	obst,
	invertDirect,
	invertControls,
	none
};
#endif