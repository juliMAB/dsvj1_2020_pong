#include "menu.h"


namespace menu
{
	namespace scenes
	{
		
		MENU currentMenu;
	}
	namespace key_change
	{
		BOTONS numbers = BOTONS::NONE;
		int timing = 0;
	}
	
	extern void init()
	{
		scenes::currentMenu = scenes::MENU::INTRO;
	}
	extern void update()
	{
		switch (scenes::currentMenu)
		{
		case scenes::MENU::INTRO:
			if (GetKeyPressed()) scenes::currentMenu = scenes::MENU::GENERAL;
			break;
		case scenes::MENU::GENERAL:
			if (IsKeyPressed('U')) scenes::currentMenu = scenes::MENU::CONFIGPADS;
			if (IsKeyPressed('J')) scenes::currentMenu = scenes::MENU::OPTIONS;
			if (IsKeyPressed('N')) scenes::currentMenu = scenes::MENU::CREDITS;
			if (IsKeyPressed(KEY_ESCAPE)) SetExitKey(KEY_ESCAPE);
			break;
		case scenes::MENU::CONFIGPADS:
			if (IsKeyPressed('N')) {

				gameplay::scenes::currentGame = gameplay::scenes::GAME::GAME;
				pong::game::currentScene = pong::game::Scene::Game;//game = GAME::RESET; //aca deveria activar el game.
				Reset::ResetMatch(gameplay::entities::ball1, gameplay::entities::ball2, gameplay::entities::pj1, gameplay::entities::pj2);
			}

			if (IsKeyPressed('B')) scenes::currentMenu = scenes::MENU::GENERAL;
			if (IsKeyPressed(gameplay::entities::pj1.keyUp)) change_color::ChangeColorUP(gameplay::entities::pj1.Ncolor,gameplay::entities::pj1.color);
			if (IsKeyPressed(gameplay::entities::pj1.keyDown)) change_color::ChangeColorDown(gameplay::entities::pj1.Ncolor, gameplay::entities::pj1.color);
			if (IsKeyPressed(gameplay::entities::pj2.keyUp)) change_color::ChangeColorUP(gameplay::entities::pj2.Ncolor, gameplay::entities::pj2.color);
			if (IsKeyPressed(gameplay::entities::pj2.keyDown)) change_color::ChangeColorDown(gameplay::entities::pj2.Ncolor, gameplay::entities::pj2.color);
			if (IsKeyPressed('X'))  gameplay::entities::bot_active = !gameplay::entities::bot_active;
			break;
		case scenes::MENU::OPTIONS:
			changeKeys::changeKeys(gameplay::entities::pj1.keyUp, gameplay::entities::pj1.keyDown, gameplay::entities::pj2.keyUp, gameplay::entities::pj2.keyDown,key_change::numbers,key_change::timing);
			key_change::timing--;
			if (key_change::timing < 0)
			{
				key_change::numbers = BOTONS::NONE;
			}
			if (IsKeyPressed('B'))scenes::currentMenu = scenes::MENU::GENERAL;
			break;
		case scenes::MENU::CREDITS:
			if (IsKeyPressed('B')) scenes::currentMenu = scenes::MENU::GENERAL;
			break;
			
		}
	}
	extern void draw()
	{
		using namespace draw_menus;
		using namespace gameplay;
		switch (scenes::currentMenu)
		{
		case scenes::MENU::INTRO:
			DrawIntro();
			break;
		case scenes::MENU::GENERAL:
			DrawGeneral();
			break;
		case scenes::MENU::CONFIGPADS:
			DrawConfigPads(entities::pj1,entities::pj2,entities::bot_active);
			break;
		case scenes::MENU::OPTIONS:
			DrawOpciones(entities::pj1, entities::pj2);
			break;
		case scenes::MENU::CREDITS:
			DrawCredits();
			break;
		}
	}
	extern void deinit()
	{

	}
}