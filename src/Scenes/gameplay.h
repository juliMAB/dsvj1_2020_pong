#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "controler/game.h"

#include "Entities/ball.h"
#include "Entities/Player.h"
#include "Entities/Screen.h"
#include "Entities/PowerUps.h"

#include "Gameplay/logic.h"
#include "Gameplay/MovePlayers.h"

#include "Draws/Draw.h"

#include "controler/game.h"

namespace gameplay
{
	namespace entities
	{
		extern BALL ball1;
		extern BALL ball2;
		extern PLAYER pj1, pj2;
		extern powers::entity::POWER powerup;
		extern powers::time::POWERxTIME pxt;
		extern bool bot_active;
		extern Rectangle highStop;
		extern Rectangle lowStop;

	}
	namespace scenes
	{
		enum class GAME
		{
			GAME,
			RESET,
			END,
			PAUSE,
			NONE
		};
		extern GAME currentGame;
	}
	namespace players
	{
		enum class PLAYERS
		{
			ONE,
			TWO,
			NONE
		};
		extern PLAYERS last_touch;
		extern PLAYERS goal;
		extern PLAYERS win;
		
	}

	extern void init();
	extern void update();
	extern void draw();
	extern void deinit();
}
#endif