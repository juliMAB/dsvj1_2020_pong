#include "logic.h"
using namespace powers;
using namespace entity;
using namespace time;

namespace Logic
{
	//Cambiar la direccion dependiendo de donde recibio el impacto la pelota.
	void ChangeDirection(BALL& ball,POWERxTIME ptx)
	{
		if (ptx.IDirect == false)
		{
			switch (ball.Shock)
			{
			case SHOCK::DOWN:
				if (ball.direction == DIRECTION::DOWNRIGHT)
				{
					ball.direction = DIRECTION::UPRIGHT;
				}
				else if (ball.direction == DIRECTION::DOWNLEFT)
				{
					ball.direction = DIRECTION::UPLEFT;
				}
				break;
			case SHOCK::RIGHT:
				if (ball.direction == DIRECTION::DOWNRIGHT)
				{
					ball.direction = DIRECTION::DOWNLEFT;
				}
				else if (ball.direction == DIRECTION::UPRIGHT)
				{
					ball.direction = DIRECTION::UPLEFT;
				}
				break;
			case SHOCK::UP:
				if (ball.direction == DIRECTION::UPRIGHT)
				{
					ball.direction = DIRECTION::DOWNRIGHT;
				}
				else if (ball.direction == DIRECTION::UPLEFT)
				{
					ball.direction = DIRECTION::DOWNLEFT;
				}
				break;
			case SHOCK::LEFT:
				if (ball.direction == DIRECTION::UPLEFT)
				{
					ball.direction = DIRECTION::UPRIGHT;
				}
				else if (ball.direction == DIRECTION::DOWNLEFT)
				{
					ball.direction = DIRECTION::DOWNRIGHT;
				}
				break;
			}
		}
		else
		{
			switch (ball.Shock)
			{
			case SHOCK::DOWN:
				if (ball.direction == DIRECTION::DOWNRIGHT)
				{
					ball.direction = DIRECTION::UPRIGHT;
				}
				else if (ball.direction == DIRECTION::DOWNLEFT)
				{
					ball.direction = DIRECTION::UPLEFT;
				}
				break;
			case SHOCK::RIGHT:
				if (ball.direction == DIRECTION::DOWNRIGHT)
				{
					ball.direction = DIRECTION::UPLEFT;
				}
				else if (ball.direction == DIRECTION::UPRIGHT)
				{
					ball.direction = DIRECTION::DOWNLEFT;
				}
				break;
			case SHOCK::UP:
				if (ball.direction == DIRECTION::UPRIGHT)
				{
					ball.direction = DIRECTION::DOWNRIGHT;
				}
				else if (ball.direction == DIRECTION::UPLEFT)
				{
					ball.direction = DIRECTION::DOWNLEFT;
				}
				break;
			case SHOCK::LEFT:
				if (ball.direction == DIRECTION::UPLEFT)
				{
					ball.direction = DIRECTION::DOWNRIGHT;
				}
				else if (ball.direction == DIRECTION::DOWNLEFT)
				{
					ball.direction = DIRECTION::UPRIGHT;
				}
				break;
			}
		}
		
	}
	void CheckCollisionNoPower(BALL& ball, PLAYER& pj1, PLAYER& pj2, Rectangle highStop, Rectangle lowStop, POWERxTIME pxt)
	{

			if (CheckCollisionCircleRec(ball.pos, ball.radio, pj2.pos)) //choca con la paleta 2.
			{
				ball.Shock = SHOCK::RIGHT;
				ball.color = pj2.color;
				Logic::ChangeDirection(ball,pxt);
				ball.speed += ball.MoreSpeed;
				
			}
			if (CheckCollisionCircleRec(ball.pos, ball.radio, pj1.pos)) //choca con la paleta 1.
			{
				ball.Shock = SHOCK::LEFT;
				ball.color = pj1.color;
				Logic::ChangeDirection(ball,pxt);
				ball.speed += ball.MoreSpeed;
				
			}
			if (CheckCollisionCircleRec(ball.pos, ball.radio, highStop))
			{

				ball.Shock = SHOCK::UP;
				Logic::ChangeDirection(ball,pxt);
				ball.speed += ball.MoreSpeed;

			}
			if (CheckCollisionCircleRec(ball.pos, ball.radio, lowStop))
			{

				ball.Shock = SHOCK::DOWN;
				Logic::ChangeDirection(ball,pxt);
				ball.speed += ball.MoreSpeed;

			}

	}
	void ConfirmarGoal(PLAYER& pj1, PLAYER& pj2, BALL& ball)
	{
		if (ball.pos.x <= 0)
		{
			pj2.puntaje++;
			Reset::ResetBall(ball);
			Reset::ResetPlayersPowers(pj1, pj2);
			ball.existo = false;
		}
		if (ball.pos.x >= screenWidth)
		{
			pj1.puntaje++;
			Reset::ResetBall(ball);
			Reset::ResetPlayersPowers(pj1,pj2);
			ball.existo = false;
		}
		
	}
	void MoveBall(BALL& ball)
	{
		switch (ball.direction)
		{
		case DIRECTION::DOWNRIGHT:
			ball.pos.x += ball.speed;
			ball.pos.y += ball.speed;
			break;
		case DIRECTION::DOWNLEFT:
			ball.pos.x -= ball.speed;
			ball.pos.y += ball.speed;
			break;
		case DIRECTION::UPRIGHT:
			ball.pos.x += ball.speed;
			ball.pos.y -= ball.speed;
			break;
		case DIRECTION::UPLEFT:
			ball.pos.x -= ball.speed;
			ball.pos.y -= ball.speed;
			break;
		default:
			break;
		}
	}
	PLAYERS ConfirmWinerR(int puntaje1, int puntaje2,short ptoWin)
	{
		if (puntaje1 > ptoWin)
		{
			return PLAYERS::ONE;
		}
		else if (puntaje2 > ptoWin)
		{
			return PLAYERS::TWO;
		}
		else
		{
			return PLAYERS::NONE;
		}
	}

	namespace power
	{

		void InvertControls(POWERxTIME& pxt); // Se usa en givepower.
		void ActiveObstcles(POWERxTIME& pxt); // Se usa en givepower.
		void InvertDirect(POWERxTIME& pxt);   // Se usa en givepower.

		//el primordial. //solo collition de la bola que da el power up.
		void PowerCollition(POWER& power, BALL& ball1, BALL& ball2, PLAYER& pj1, PLAYER& pj2, POWERxTIME& pxt);
		
		//colisi�n.
		void collitionObstacles(BALL& ball,BALL& ball2, time::POWERxTIME pxt)
		{
			if (pxt.obstB)
			{
				if (CheckCollisionCircleRec(ball.pos, ball.radio, pxt.obst.rec1))
				{
					if (!ball.coli)
					{
						if (ball.direction == DIRECTION::DOWNLEFT || ball.direction == DIRECTION::UPLEFT) //viene por la por la izquierda.
						{
							ball.Shock = SHOCK::LEFT; //choque de derecha.
							ChangeDirection(ball, pxt);
							ball.speed += ball.MoreSpeed;
							ball.coli = true;
						}
						else
						{
							ball.Shock = SHOCK::RIGHT; //choque de izquierda.
							ChangeDirection(ball, pxt);
							ball.speed += ball.MoreSpeed;
							ball.coli = true;
						}
					}
				}
				else
				{
					ball.coli = false;
				}
				if (CheckCollisionCircleRec(ball.pos, ball.radio, pxt.obst.rec2)) //choca con la paleta 1.
				{
					if (!ball.coli)
					{
						if (ball.direction == DIRECTION::DOWNLEFT || ball.direction == DIRECTION::UPLEFT) //viene por la por la izquierda.
						{
							ball.Shock = SHOCK::LEFT; //choque de derecha.
							ChangeDirection(ball, pxt);
							ball.speed += ball.MoreSpeed;
							ball.coli = true;
						}
						else
						{
							ball.Shock = SHOCK::RIGHT; //choque de izquierda.
							ChangeDirection(ball, pxt);
							ball.speed += ball.MoreSpeed;
							ball.coli = true;
						}
					}
					else
					{
						ball.coli = false;
					}
					
				}
				if (CheckCollisionCircleRec(ball2.pos, ball2.radio, pxt.obst.rec1))
				{
					if (!ball2.coli)
					{
						if (ball2.direction == DIRECTION::DOWNLEFT || ball2.direction == DIRECTION::UPLEFT) //viene por la por la izquierda.
						{
							ball2.Shock = SHOCK::LEFT; //choque de derecha.
							ChangeDirection(ball2, pxt);
							ball2.speed += ball2.MoreSpeed;
							ball2.coli = true;
						}
						else
						{
							ball2.Shock = SHOCK::RIGHT; //choque de izquierda.
							ChangeDirection(ball2, pxt);
							ball2.speed += ball2.MoreSpeed;
							ball2.coli = true;
						}
					}
					else
					{
						ball2.coli = false;
					}
					

				}
				if (CheckCollisionCircleRec(ball2.pos, ball2.radio, pxt.obst.rec2)) //choca con la paleta 1.
				{
					if (!ball2.coli)
					{
						if (ball2.direction == DIRECTION::DOWNLEFT || ball2.direction == DIRECTION::UPLEFT) //viene por la por la izquierda.
						{
							ball2.Shock = SHOCK::LEFT; //choque de derecha.
							ChangeDirection(ball2, pxt);
							ball2.speed += ball2.MoreSpeed;
							ball2.coli = true;
						}
						else
						{
							ball2.Shock = SHOCK::RIGHT; //choque de izquierda.
							ChangeDirection(ball2, pxt);
							ball2.speed += ball2.MoreSpeed;
							ball2.coli = true;
						}
					}
					else
					{
						ball2.coli = true;
					}
					
				}
			}
		}
		void collitionShild(BALL& ball, PLAYER& pj1, PLAYER& pj2,powers::time::POWERxTIME pxt)
		{
			if (pj1.shield.shieldBool)
			{
				if (CheckCollisionCircleRec(ball.pos, ball.radio, pj1.shield.shield))
				{
					pj1.shield.shieldBool = false;
					ball.Shock = SHOCK::LEFT;
					ChangeDirection(ball,pxt);
				}
			}
			if (pj2.shield.shieldBool)
			{
				if (CheckCollisionCircleRec(ball.pos, ball.radio, pj2.shield.shield))
				{
					pj2.shield.shieldBool = false;
					ball.Shock = SHOCK::RIGHT;
					ChangeDirection(ball,pxt);
				}
			}

		}

		//la pelota que vuela que podes agarrar para tomar el poder.
		void SpawnRandomPower(POWER& power)
		{
			if (power.time<-300) //durante 5 segundos no existe
			{
				if (!power.exist) // si no existe un power up.
				{
					power.pos = { (float)(rand() % ((screenWidth - 50) - 50) + 50),(float)(rand() % ((screenHeight - 50) - 50) + 50) };
					power.exist = true;
					power.time = 300; // existe durante 5 segundos.
					power.power_actual = (POWER_UPS)(rand() % 6); // y toca un poder random.
				}
			}
			
		}
		void DeSpawnRandomPower(POWER& power)
		{
			if (power.time<0)//si pasan 5 segundos se existencia.
			{
				if (power.exist) // y existe.
				{
					power.exist = false; //deja de existir.
				}
			}
			
		}
		void PowerClockUpdate(POWER& power)
		{
			power.time--;
		}

		//para detectar al ultimo jugador en tocar x pelota.
		void LastTouch(PLAYER pj1, PLAYER pj2, BALL& ball1, BALL& ball2 ) 
		{
			if (CheckCollisionCircleRec(ball1.pos,ball1.radio,pj1.pos))
			{
				ball1.lastPlayerTouch = PLAYERS::ONE;
			}
			if (CheckCollisionCircleRec(ball2.pos, ball2.radio, pj1.pos))
			{
				ball2.lastPlayerTouch = PLAYERS::ONE;
			}
			if (CheckCollisionCircleRec(ball1.pos, ball1.radio, pj2.pos))
			{
				ball1.lastPlayerTouch = PLAYERS::TWO;
			}
			if (CheckCollisionCircleRec(ball2.pos, ball2.radio, pj2.pos))
			{
				ball2.lastPlayerTouch = PLAYERS::TWO;
			}
		}
		
		// Se usa en givepower.
		void GivePowerUpShield(BALL ball,PLAYER& pj, PLAYER& pj2)
		{
			
			if (ball.lastPlayerTouch == PLAYERS::ONE)
			{
				pj.shield.shieldBool = true;
			}
			else if (ball.lastPlayerTouch == PLAYERS::TWO)
			{
				pj2.shield.shieldBool = true;
			}
		}  // Se usa en givepower.
		void GivePowerUpSpeed(BALL ball,PLAYER& pj, PLAYER& pj2)
		{
			if (ball.lastPlayerTouch == PLAYERS::ONE)
			{
				pj.speed++;
			}
			else if (ball.lastPlayerTouch == PLAYERS::TWO)
			{
				pj2.speed++;
			}
		}  // Se usa en givepower.
		void GivePowerUpBig(BALL ball, PLAYER& pj, PLAYER& pj2)
		{
			if (ball.lastPlayerTouch == PLAYERS::ONE)
			{
				pj.pos.height += pj.pos.height / 2; // crece la mitad.
			}
			else if (ball.lastPlayerTouch == PLAYERS::TWO)
			{
				pj2.pos.height += pj2.pos.height / 2; // crece la mitad.
			}
		}   // Se usa en givepower.

		// Se usa en PowerCollition.
		void GivePower(POWER& power, BALL ball, PLAYER& pj1, PLAYER& pj2, BALL& ball2, POWERxTIME& pxt)
		{
			POWER_UPS aumento;
			aumento = ((POWER_UPS)(rand() % (int)POWER_UPS::none));
			switch (aumento)
			{
			case POWER_UPS::shield:
				GivePowerUpShield(ball, pj1, pj2);
				break;
			case POWER_UPS::bigpad:
				GivePowerUpBig(ball, pj1, pj2);
				break;
			case POWER_UPS::morespeed:
				GivePowerUpSpeed(ball, pj1, pj2);
				break;
			case POWER_UPS::twoball:
				ball2.existo = true;
				break;
			case POWER_UPS::obst:
				if (pxt.duration <= 0)
				{
					ActiveObstcles(pxt);
				}
				else
				{
					PowerCollition(power, ball, ball2, pj1, pj2, pxt);
				}
				break;
			case POWER_UPS::invertDirect:
				if (pxt.duration <= 0)
				{
					InvertDirect(pxt);
				}
				else
				{
					PowerCollition(power, ball, ball2, pj1, pj2, pxt);
				}
				break;
			case POWER_UPS::invertControls:
				if (pxt.duration <= 0)
				{
					InvertControls(pxt);
				}
				else
				{
					PowerCollition(power, ball, ball2, pj1, pj2, pxt);
				}
				break;
			case POWER_UPS::none:
				break;

			}
		}

		//el primordial.
		void PowerCollition(POWER& power, BALL& ball1, BALL& ball2, PLAYER& pj1, PLAYER& pj2, POWERxTIME& pxt)
		{
			if (power.exist == true)
			{
				if (CheckCollisionCircles(ball1.pos, (float)ball1.radio, power.pos, (float)power.radio))
				{
					power.exist = false;
					GivePower(power, ball1, pj1, pj2, ball2, pxt);
				}
				if (CheckCollisionCircles(ball2.pos, (float)ball2.radio, power.pos, (float)power.radio))
				{
					power.exist = false;
					GivePower(power, ball2, pj1, pj2, ball2, pxt);
				}
			}

		}
		
		// Se usa en givepower.
		void InvertDirect(POWERxTIME& pxt)
		{
			pxt.IDirect = true;
			pxt.duration = 900;
		}
		void ActiveObstcles(POWERxTIME& pxt)
		{
			pxt.obstB = true;
			pxt.duration = 900;
		}
		void InvertControls(POWERxTIME& pxt)
		{
			pxt.duration = 600;
			pxt.IControls = true;
		}
		

		//update.
		void UpdatePxT(POWERxTIME& pxt)
		{
			if (pxt.duration>0)
			{
				pxt.duration--;
			}
			if (pxt.duration==0)
			{
				pxt.IDirect = false;
				pxt.obstB = false;
				pxt.IControls = false;
			}
		}
	}


}
