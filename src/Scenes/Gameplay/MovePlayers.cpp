
#include "MovePlayers.h"

namespace MovePlayer
{
    //movimiento del bot, se usa adentro de MovePlayers.
    void BotFollowBall(PLAYER& pj, BALL ball)
    {
        float middlePointPad = pj.pos.y + pj.pos.height - pj.pos.height / 2;
        if (ball.pos.y < middlePointPad)
        {
            pj.pos.y -= pj.speed;
        }
        else if (ball.pos.y > middlePointPad)
        {
            pj.pos.y += pj.speed;
        }

    }

    //movimiento de los pj: normal y invertido.
    void MovePlayersS(PLAYER& pj1, PLAYER& pj2, bool bot, BALL ball, BALL ball2)//pj1Y,pj2y,speed,hight,screenheig
    {
        if (pj1.pos.y >= 0)
        {
            if (IsKeyDown(pj1.keyUp)) pj1.pos.y -= pj1.speed;
        }
        if (pj1.pos.y <= screenHeight - pj1.pos.height)
        {
            if (IsKeyDown(pj1.keyDown)) pj1.pos.y += pj1.speed;
        }
        if (!bot)
        {
            if (pj2.pos.y >= 0)
            {
                if (IsKeyDown(pj2.keyUp)) pj2.pos.y -= pj2.speed;
            }
            if (pj2.pos.y <= screenHeight - pj2.pos.height)
            {
                if (IsKeyDown(pj2.keyDown)) pj2.pos.y += pj2.speed;
            }
        }
        else
        {

            if (ball.direction == DIRECTION::DOWNRIGHT || ball.direction == DIRECTION::UPRIGHT)
            {
                if (ball.pos.x < ball2.pos.x && ball2.existo)
                {
                    BotFollowBall(pj2, ball2);
                }
                else
                {
                    BotFollowBall(pj2, ball);
                }            
            }
        }
    }
    void MovePlayersSinvrt(PLAYER& pj1, PLAYER& pj2, bool bot, BALL ball, BALL ball2)//pj1Y,pj2y,speed,hight,screenheig
    {
        if (pj1.pos.y >= 0)
        {
            if (IsKeyDown(pj1.keyDown)) pj1.pos.y -= pj1.speed;
        }
        if (pj1.pos.y <= screenHeight - pj1.pos.height)
        {
            if (IsKeyDown(pj1.keyUp)) pj1.pos.y += pj1.speed;
        }
        if (!bot)
        {
            if (pj2.pos.y >= 0)
            {
                if (IsKeyDown(pj2.keyDown)) pj2.pos.y -= pj2.speed;
            }
            if (pj2.pos.y <= screenHeight - pj2.pos.height)
            {
                if (IsKeyDown(pj2.keyUp)) pj2.pos.y += pj2.speed;
            }
        }
        else
        {

            if (ball.direction == DIRECTION::DOWNRIGHT || ball.direction == DIRECTION::UPRIGHT)
            {
                if (ball.pos.x < ball2.pos.x && ball2.existo)
                {
                    BotFollowBall(pj2, ball2);
                }
                else
                {
                    BotFollowBall(pj2, ball);
                }
            }
        }
    }
}

