#ifndef LOGIC_H
#define LOGIC_H

#include "Entities/ball.h"
#include "Entities/Player.h"
#include "Entities/PowerUps.h"

#include "ENUMS/direction.h"
#include "ENUMS/shock.h"
#include "ENUMS/Winers.h"

#include "Scenes/Reset/Resets.h"

#include "Scenes/gameplay.h"

namespace Logic
{

	void CheckCollisionNoPower(BALL& ball, PLAYER& pj1, PLAYER& pj2, Rectangle highStop, Rectangle lowStop,powers::time::POWERxTIME pxt);

	void ConfirmarGoal(PLAYER& pj1, PLAYER& pj2, BALL& ball);

	void MoveBall(BALL& ball);

	PLAYERS ConfirmWinerR(int puntaje1, int puntaje2, short ptoWin);


	
	namespace power
	{
		//collitions.
		void collitionObstacles(BALL& ball, BALL& ball2, powers::time::POWERxTIME pxt);

		void collitionShild(BALL& ball, PLAYER& pj1, PLAYER& pj2, powers::time::POWERxTIME pxt);

		//la pelota que vuela que podes agarrar para tomar el poder.
		void SpawnRandomPower(powers::entity::POWER& power);
		void DeSpawnRandomPower(powers::entity::POWER& power);
		void PowerClockUpdate(powers::entity:: POWER& power);

		//para detectar al ultimo jugador en tocar x pelota.
		void LastTouch(PLAYER pj1, PLAYER pj2, BALL& ball1, BALL& ball2);

		//colition con la bola en el medio.
		void PowerCollition(powers::entity::POWER& power, BALL& ball1, BALL& ball2, PLAYER& pj1, PLAYER& pj2, powers::time::POWERxTIME& pxt);
		
		//update.
		void UpdatePxT(powers::time::POWERxTIME& pxt);
	
	
	}
}
#endif