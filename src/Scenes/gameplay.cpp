#include "gameplay.h"


namespace gameplay
{
	//generalidades.
	//para definir el ganador.
	PLAYERS win;
	//para jugar a 10 puntos.
	short point_to_mach = 10;
	//para el relojito de el principio del juego.
	//se puede setear en 0 para no tener que esperar.
	int timer=180; //60 frames 1 sg, 120_2,180_3;
	
	
	namespace entities //las entidades en el juego.
	{
		BALL ball1;
		BALL ball2;
		PLAYER pj1, pj2;

		//powerups.
		powers::entity::POWER powerup;
		powers::time::POWERxTIME pxt;

		//bot.
		bool bot_active;

		//topes alto y bajo.
		Rectangle highStop;
		Rectangle lowStop;

	}


	namespace scenes //escena actual del juego. dentro del mismo juego.
	{
		GAME currentGame;
	}


	extern void init() 
	{
		//inicializo todo lo que voy a usar en el juego.
		using namespace entities;
		bot_active = false;
		timer = 180;
		powerup.time = 300;
		highStop = { 0, 0, screenWidth, 10 };
		lowStop = { 0, screenHeight , screenWidth, 10 };
		ball1 =
		{
			{middlescreenWidth,middlescreenHeight},
			(DIRECTION)(rand() % 4),
			false,
			3,
			SHOCK::DOWN,
			10,
			VIOLET,
			.09f,
			true,
		};
		ball2 =
		{
			{middlescreenWidth,middlescreenHeight},
			(DIRECTION)(rand() % 4),
			false,
			3,
			SHOCK::DOWN,
			10,
			VIOLET,
			.09f,
			false,
		};
		pj1 =
		{
			{10,middlescreenHeight, screenWidth / 70, screenHeight / 7},
			4,
			0,
			RED,
			0,
			{ {0,0, screenWidth / 90, screenHeight},
			false
			},
		};
		pj2 =
		{
			{ screenWidth - entities::pj2.pos.width,middlescreenHeight - 10, screenWidth / 70, screenHeight / 7 },
			4,
			0,
			RED,
			0,
			{{screenWidth - entities::pj2.shield.shield.width,0},
			false,},
			'O',
			'L'
		};
		pxt.obst.rec1 = { (screenWidth / 10) * 7,(screenHeight / 10) * 2,screenWidth / 70,screenHeight / 7 };
		pxt.obst.rec2 = { (screenWidth / 10) * 2,(screenHeight / 10) * 7,screenWidth / 70,screenHeight / 7 };
	}
	extern void update()
	{
		
		switch (gameplay::scenes::currentGame)
		{
		case::gameplay::scenes::GAME::GAME: //aca va ir todo el juego.

			if (timer > 0)
			{
				timer--;
			}
			else
			{
				//PAUSE.
				if (IsKeyPressed('P')) gameplay::scenes::currentGame = gameplay::scenes::GAME::PAUSE;

				//Movimiento de los pj.
				if (entities::pxt.IControls == false)
				{
					MovePlayer::MovePlayersS(entities::pj1, entities::pj2, entities::bot_active, entities::ball1, entities::ball2);

				}
				else
				{
					MovePlayer::MovePlayersSinvrt(entities::pj1, entities::pj2, entities::bot_active, entities::ball1, entities::ball2);
				}

				//LOGICA DE LA BOLA2.

				if (entities::ball2.existo)
				{
					//entities::ball2.coli = false;

					Logic::CheckCollisionNoPower(entities::ball2, entities::pj1, entities::pj2, entities::highStop, entities::lowStop,entities::pxt);

					Logic::power::collitionShild(entities::ball2, entities::pj1, entities::pj2,entities::pxt);

					Logic::ConfirmarGoal(entities::pj1, entities::pj2, entities::ball2);

					Logic::MoveBall(entities::ball2);
				}

				//LOGICA DE LA BOLA1.
				{
					//entities::ball1.coli = false;

					Logic::CheckCollisionNoPower(entities::ball1, entities::pj1, entities::pj2, entities::highStop, entities::lowStop,entities::pxt);

					Logic::power::collitionShild(entities::ball1, entities::pj1, entities::pj2, entities::pxt);

					Logic::ConfirmarGoal(entities::pj1, entities::pj2, entities::ball1);

					Logic::MoveBall(entities::ball1);
				}


				//LOGICA DE VICTORIA.
				if (Logic::ConfirmWinerR(entities::pj1.puntaje, entities::pj2.puntaje, gameplay::point_to_mach) != PLAYERS::NONE)
				{
					gameplay::scenes::currentGame = gameplay::scenes::GAME::END;
				}

				//LOGICA DE POWERS.
				{
					//colisi�n.
					if (entities::pxt.obstB)
					{
						Logic::power::collitionObstacles(entities::ball1, entities::ball2, entities::pxt);

					}

					//logica de pickup.
					Logic::power::SpawnRandomPower(entities::powerup);

					Logic::power::PowerClockUpdate(entities::powerup);

					Logic::power::DeSpawnRandomPower(entities::powerup);

					//updates.
					Logic::power::LastTouch(entities::pj1, entities::pj2, entities::ball1, entities::ball2);

					Logic::power::UpdatePxT(entities::pxt);

					//La colision principal entre las bolas con el powerup y sus respectivos dadores a pjs.
					Logic::power::PowerCollition(entities::powerup, entities::ball1, entities::ball2, entities::pj1, entities::pj2, entities::pxt);
				}
				break;
		case::gameplay::scenes::GAME::PAUSE: //aca todo se va a poner en 0.
			if (IsKeyPressed('P')) scenes::currentGame = scenes::GAME::GAME;
			if (IsKeyPressed('R')) scenes::currentGame = scenes::GAME::RESET;
			if (IsKeyPressed('G'))
			{
				menu::scenes::currentMenu = menu::scenes::MENU::GENERAL;
				pong::game::currentScene = pong::game::Scene::Menu;
			}

			//el juego esta pausado y solo se pausa una vez, por lo tanto solo en init.
			break;
		case::gameplay::scenes::GAME::END: // aca voy a mostrar quien gano por un periodo y volver al menu.
			if (IsKeyPressed('N'))
			{
				menu::scenes::currentMenu = menu::scenes::MENU::GENERAL;
				pong::game::currentScene = pong::game::Scene::Menu;
			}
			break;
		case::gameplay::scenes::GAME::RESET: //aca no va a pasar nada, porque el reset solo va a ser en el init.
			Reset::ResetBall(entities::ball1);
			Reset::ResetBall(entities::ball2);
			entities::ball2.existo = false;
			Reset::ResetPlayers(entities::pj1, entities::pj2);
			scenes::currentGame = scenes::GAME::GAME;
			timer = 180;
			break;
		case::gameplay::scenes::GAME::NONE: //aca tampoco pasa nada, solo para rellenar el default.
			break;
			}
		}
			
	}
	extern void draw() 
	{
		using namespace draw_game;
		using namespace gameplay;
		switch (gameplay::scenes::currentGame)
		{
		case::gameplay::scenes::GAME::GAME: //aca va ir todo el juego.
			DrawGame(entities::ball1, entities::pj1, entities::pj2, entities::pxt, entities::ball2, timer,entities::powerup);
			break;
		case::gameplay::scenes::GAME::PAUSE: //aca todo se va a poner en 0.
			DrawPause();
			break;
		case::gameplay::scenes::GAME::END: // aca voy a mostrar quien gano por un periodo y volver al menu.
			DrawEnd(win);
			break;
		case::gameplay::scenes::GAME::RESET: //aca no va a pasar nada, porque el reset solo va a ser en el init.
			
			break;
		case::gameplay::scenes::GAME::NONE: //aca tampoco pasa nada, solo para rellenar el default.
			break;
		}
	}
	extern void deinit() 
	{

	}
}