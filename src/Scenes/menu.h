#ifndef MENU_H
#define MENU_H

#include "menu/changeKeys/change_keys.h"
#include "menu/changeColor/change_color.h"

#include "gameplay.h"

#include "controler/game.h"

namespace menu
{
	namespace scenes
	{
		enum class MENU
		{
			INTRO,
			GAME,
			OPTIONS,
			GENERAL,
			PAUSE,
			EXIT,
			CONFIGPADS,
			CREDITS
		};
		extern MENU currentMenu;
	}
	namespace key_change
	{
		extern short number;
		extern char saveKey;
		extern bool saveKeyBool;


	}
	extern void init();
	extern void update();
	extern void draw();
	extern void deinit();
}
#endif