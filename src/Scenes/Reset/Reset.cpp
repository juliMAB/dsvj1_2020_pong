#include "Resets.h"

namespace Reset
{
	void ResetBall(BALL& ball) //llevar la pelota al centro de la cancha y volverla a su estado inicial.
	{
		srand(time(NULL));
		ball.pos.x = (float)screenWidth / 2;
		ball.pos.y = (float)screenHeight / 2;
		ball.coli = false;
		ball.speed = 3;
		ball.direction = (DIRECTION)(rand() % 4);
		ball.color = VIOLET;
	}

	//Se usan en RESETPLAYES.
	void ResetPlayersOne(PLAYER& pj)
	{
		pj.pos = { 10,screenHeight / 2 - pj.pos.height / 2,pj.pos.width,pj.pos.height }; //x,y,width,hight.
		pj.speed = 4; //movement.
		pj.puntaje = 0;
	}
	void ResetPlayersTwo(PLAYER& pj)
	{
		pj.pos = { screenWidth - pj.pos.width - 10, screenHeight / 2 - pj.pos.height / 2,pj.pos.width,pj.pos.height }; //x,y,width,hight.
		pj.speed = 4; //movement.
		pj.puntaje = 0;
	}
	
	void ResetPlayersPowers(PLAYER& pj, PLAYER& pj2)
	{
		pj.shield.shieldBool = false; //sacar escudo.
		pj.pos.height = screenHeight / 7; //volver a tama�o normal.
		pj.speed = 4; // 4 por defecto.
		pj2.shield.shieldBool = false; //sacar escudo.
		pj2.pos.height = screenHeight / 7; //volver a tama�o normal.
		pj2.speed = 4; // 4 por defecto.
	}

	void ResetPlayers(PLAYER& pj1, PLAYER& pj2)
	{
		ResetPlayersOne(pj1);
		ResetPlayersTwo(pj2);
	}

	void ResetMatch(BALL& ball,BALL& ball2, PLAYER& pj1, PLAYER& pj2)
	{
		ResetPlayers(pj1, pj2);
		ResetBall(ball);	
		ResetBall(ball2);
		ball2.existo = false;
	}

}