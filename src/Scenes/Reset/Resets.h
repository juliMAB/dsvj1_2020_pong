#ifndef RESETS_H
#define RESETS_H
#include "Entities/Player.h"
#include "Entities/Ball.h"
#include "ENUMS/direction.h"
#include <time.h>
#include <stdlib.h>
namespace Reset
{
	void ResetBall(BALL& ball);

	void ResetPlayers(PLAYER& pj1, PLAYER& pj2);

	void ResetMatch(BALL& ball, BALL& ball2, PLAYER& pj1, PLAYER& pj2);

	void ResetPlayersPowers(PLAYER& pj, PLAYER& pj2);

}

#endif // !RESETS_H