#include "change_color.h"


namespace change_color
{
	Color colors[6]{ RED,BLUE,YELLOW,GREEN,ORANGE,GRAY };
	void ChangeColorUP(short & nc, Color & p) //numeroColor y colorPlayer.
	{
		if (nc != 5)
		{
			nc++;

			p = colors[nc];
		}

	}

	void ChangeColorDown(short& nc, Color& p)
	{
		if (nc != 0)
		{
			nc--;

			p = colors[nc];
		}
	}
}
