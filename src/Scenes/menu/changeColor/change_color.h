#ifndef CHANGE_COLOR_H
#define CHANGE_COLOR_H

#include "raylib.h"

namespace change_color
{
	void ChangeColorUP(short& nc, Color& p);
	void ChangeColorDown(short& nc, Color& p);
}
#endif