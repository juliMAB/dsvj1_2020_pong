#ifndef CHANGE_KEYS_H
#define CHANGE_KEYS_H

#include "raylib.h"

#include "ENUMS/Winers.h"

namespace changeKeys
{
	void changeKeys(char& up1, char& down1, char& up2, char& down2, BOTONS& ChangeKeys,int & timing);
}
#endif