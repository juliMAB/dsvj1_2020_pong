#include "change_keys.h"
namespace changeKeys
{
   
    void GetInputChange(char& key)
    {
        int ingreso = GetKeyPressed();
        if (ingreso>32 && ingreso<254)
        {
            if (ingreso != 0 && ingreso != '1' && ingreso != '2' && ingreso != '3' && ingreso != '4')
            {
                if (ingreso>=(int)'a')
                {
                    ingreso = ingreso - 32;
                }
                key = (char)ingreso;
            }
            
        }
    }
    void changeKeys(char& up1, char& down1, char& up2, char& down2, BOTONS& ChangeKeys, int& timing)
    {
        if (IsKeyPressed('1')||ChangeKeys==BOTONS::ONE)
        {
            if (ChangeKeys== BOTONS::ONE && timing>0)
            {
                GetInputChange(up1);
               
            }
            else if (timing<0)
            {
                ChangeKeys = BOTONS::ONE;
                timing = 120;
            }  
        }
        if (IsKeyPressed('2') || ChangeKeys == BOTONS::TWO)
        {
            if (ChangeKeys == BOTONS::TWO && timing > 0)
            {
                GetInputChange(down1);

            }
            else if (timing < 0)
            {
                ChangeKeys = BOTONS::TWO;
                timing = 120;
            }
        }
        if (IsKeyPressed('3') || ChangeKeys == BOTONS::THREE)
        {
            if (ChangeKeys == BOTONS::THREE && timing > 0)
            {
                GetInputChange(up2);

            }
            else if (timing < 0)
            {
                ChangeKeys = BOTONS::THREE;
                timing = 120;
            }
        }
        if (IsKeyPressed('4')||ChangeKeys == BOTONS::FOUR)
        {
            if (ChangeKeys == BOTONS::FOUR && timing > 0)
            {
                GetInputChange(down2);

            }
            else if (timing < 0)
            {
                ChangeKeys = BOTONS::FOUR;
                timing = 120;
            }
        }
    }
}

