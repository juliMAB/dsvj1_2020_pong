#include "Draw.h"


namespace draw_menus
{
	void DrawIntro()
	{
		DrawText("PONG 1.0", screenWidth / 2 - (MeasureText("PONG 1.0", MediumSize) / 2), screenHeight / 2, MediumSize, BLACK);
		DrawText("Press any key to continue", screenWidth / 2 - (MeasureText("Press any key to continue", SmallSize) / 2), screenHeight / 2 + 40, SmallSize, BLACK);
	}
	void DrawGeneral()
	{
		DrawText("PLAY [U]", screenWidth / 2 - (MeasureText("PLAY [U]", MediumSize) / 2), screenHeight / 2 - 40, MediumSize, BLACK);

		DrawText("CONFIG [J]", screenWidth / 2 - (MeasureText("CONFIG [J]", MediumSize) / 2), screenHeight / 2, MediumSize, BLACK);

		DrawText("CREDITS [N]", screenWidth / 2 - (MeasureText("CREDITS [N]", MediumSize) / 2), screenHeight / 2 + 40, MediumSize, BLACK);

		DrawText("Exit [ESC 2 times]", screenWidth / 2 - (MeasureText("Exit [ESC 2 times]", MediumSize) / 2), screenHeight / 2 + 80, MediumSize, BLACK);
	}
	void DrawConfigPads(PLAYER& pj1, PLAYER& pj2, bool bot)
	{
		DrawRectangle(50, 50, (int)pj1.pos.width + 20, (int)pj1.pos.height + 20, pj1.color);
		DrawRectangle(screenWidth - (int)pj2.pos.width - 40, 50, (int)pj2.pos.width + 20, (int)pj2.pos.height + 20, pj2.color);

		DrawText((FormatText("This is your pad, you can change the color using %c and %c",pj1.keyUp,pj1.keyDown)), middlescreenWidth - MeasureText("This is your pad, you can change the color using A and A", MediumSize)/2 , screenHeight / 2, MediumSize, BLACK);
		DrawText((FormatText("also the other pad with %c and %c", pj2.keyUp, pj2.keyDown)), middlescreenWidth - MeasureText("also the other pad with A and A", MediumSize)/2, screenHeight / 2 + 20, MediumSize, BLACK);
		
		DrawText("Play whit bot? press [X]", middlescreenWidth - MeasureText("Play whit bot? press [X]", MediumSize)/2, screenHeight / 2 + 40, MediumSize, BLACK);
		
		if (bot) DrawText("Bot [YES]", middlescreenWidth - MeasureText("Bot [YES]", MediumSize)/2, screenHeight / 2 + 60, MediumSize, BLACK);
		else	 DrawText("Bot [NO]", middlescreenWidth - MeasureText("Bot [NO]", MediumSize)/2 , screenHeight / 2 + 60, MediumSize, BLACK);
		
		DrawText("go back [B]", 10, screenHeight - 40, MediumSize, BLACK);
		DrawText("go Next [N]", screenWidth - MeasureText("go Next [N]", MediumSize), screenHeight - 40, MediumSize, BLACK);
	}
	void DrawOpciones(PLAYER pj, PLAYER pj2)
	{
		DrawText(FormatText("Press 1 para modificar la tecla pj1_KeyUp: %c", pj.keyUp), screenWidth / 2 - (MeasureText("Press 1 para modificar la tecla pj1_KeyUp: A", MediumSize) / 2), Line5Screen, MediumSize, BLACK);
		DrawText(FormatText("Press 2 para modificar la tecla pj1_KeyDown: %c", pj.keyDown), screenWidth / 2 - (MeasureText("Press 1 para modificar la tecla pj1_KeyUp: A", MediumSize) / 2), Line5Screen * 2, MediumSize, BLACK);
		DrawText(FormatText("Press 3 para modificar la tecla pj2_KeyUp: %c", pj2.keyUp), screenWidth / 2 - (MeasureText("Press 1 para modificar la tecla pj1_KeyUp: A", MediumSize) / 2), Line5Screen * 3, MediumSize, BLACK);
		DrawText(FormatText("Press 4 para modificar la tecla pj2_KeyDown: %c", pj2.keyDown), screenWidth / 2 - (MeasureText("Press 1 para modificar la tecla pj1_KeyUp: A", MediumSize) / 2), Line5Screen * 4, MediumSize, BLACK);
		DrawText("go back [B]", 10, screenHeight - 40, MediumSize, BLACK);
	}
	void DrawCredits()
	{
		DrawText("no assets have been used at the moment", screenWidth / 2 - (MeasureText("no assets have been used at the moment", MediumSize) / 2), screenHeight / 2, MediumSize, BLACK);
		DrawText("By Julian Aguirre", screenWidth / 2 - (MeasureText("By Julian Aguirre", MediumSize) / 2), Line10Screen, MediumSize, BLACK);
		DrawText("Only use Raylib", screenWidth / 2 - (MeasureText("Only use Raylib", MediumSize) / 2), Line10Screen*2, MediumSize, BLACK);
		//DrawText("special thanks to Adrian Sgro", screenWidth / 2 - (MeasureText("special thanks to Adrian Sgro", MediumSize) / 2), Line10Screen * 3, MediumSize, BLACK);
		DrawText("version: PONG 1.0", screenWidth / 2 - (MeasureText("version: PONG 1.0", MediumSize) / 2), Line10Screen * 4, MediumSize, BLACK);

		DrawText("go back [B]", 10, screenHeight - 40, MediumSize, BLACK);
	}
	
}
namespace draw_game
{
	using namespace powers;
	void DrawField();
	void DrawBall(BALL ball);
	void DrawPlayers(PLAYER pj1, PLAYER pj2);
	void DrawPoints(PLAYER pj1, PLAYER pj2);
	void DrawShield(PLAYER pj1, PLAYER pj2);
	void DrawObstacles(time::POWERxTIME pxt);
	void DrawBall2(BALL ball2);
	void DrawPower(entity::POWER pwr);
	void DrawTimer(int timer);

	void DrawGame(BALL ball, PLAYER pj1, PLAYER pj2, powers::time::POWERxTIME pxt, BALL ball2, int timer, powers::entity::POWER pwr)
	{
		if (timer > 0)
		{
			
			DrawField();
			DrawTimer(timer);
		}
		else
		{
			DrawField();
			ClearBackground(RAYWHITE);
			DrawBall(ball);
			DrawPlayers(pj1, pj2);
			DrawPoints(pj1, pj2);
			DrawShield(pj1, pj2);
			//powers
			DrawObstacles(pxt);
			DrawBall2(ball2);
			DrawPower(pwr);
		}
	}

	void DrawPause()
	{
		DrawText("PAUSE", screenWidth / 2 - (MeasureText("PLAY [U]", MediumSize) / 2), screenHeight / 2 - 40, MediumSize, BLACK);

		DrawText("RESET [R]", screenWidth / 2 - (MeasureText("RESET [R]", SmallSize + 10) / 2), screenHeight / 2, SmallSize + 10, BLACK);

		DrawText("MENU [G]", screenWidth / 2 - (MeasureText("MENU [G]", MediumSize) / 2), screenHeight / 2 + 40, MediumSize, BLACK);
	}

	void DrawEnd(PLAYERS w)
	{
		if (w == PLAYERS::ONE)
		{
			DrawText("Player 1 Win", screenWidth - MeasureText("Player 1 Win", MediumSize) - 50, screenHeight / 2, MediumSize, BLACK);
			DrawText("go Next [N]", screenWidth - MeasureText("go Next [N]", MediumSize), screenHeight - 40, MediumSize, BLACK);

		}
		else
		{
			DrawText("Player 2 Win", screenWidth - MeasureText("Player 2 Win", MediumSize) - 50, screenHeight / 2, MediumSize, BLACK);
			DrawText("go Next [N]", screenWidth - MeasureText("go Next [N]", MediumSize), screenHeight - 40, MediumSize, BLACK);

		}
	}

	void DrawField()
	{
		DrawRectangle(0, 0, screenWidth, 5, GRAY);
		DrawRectangle(0, screenHeight-5, screenWidth, 5, GRAY);
		DrawRectangle(0, 0, 5, screenHeight, GRAY);
		DrawRectangle(screenWidth-5, 5, 5, screenHeight, GRAY);
		DrawRectangle(0, screenHeight / 8 * 3, screenWidth / 50*4, screenHeight / 3, GRAY);
		DrawRectangle(0, screenHeight / 8 * 3 + 10, screenWidth / 50 * 4 -10, screenHeight / 3 -20, WHITE);
		DrawRectangle(screenWidth - ((screenWidth / 50) * 4), screenHeight / 8 * 3, screenWidth / 50 * 4, screenHeight / 3, GRAY);
		DrawRectangle(screenWidth - ((screenWidth / 50) * 4)+10, screenHeight / 8 * 3 + 10, screenWidth / 50 * 4 - 10, screenHeight / 3 - 20, WHITE);
		DrawRectangle(middlescreenWidth - 3, 0, 6, screenHeight, GRAY);
		DrawCircle(middlescreenWidth, middlescreenHeight, 50, GRAY);
		DrawCircle(middlescreenWidth, middlescreenHeight, 40, WHITE);
	}
	using namespace powers;
	void DrawPoints(PLAYER pj1, PLAYER pj2)
	{
		DrawText(FormatText("%i", pj2.puntaje), middlescreenWidth + 100, 10, 20, DARKGRAY);
		DrawText(FormatText("%i", pj1.puntaje), middlescreenWidth - 100, 10, 20, DARKGRAY);

	}
	void DrawBall(BALL ball)
	{
		DrawCircleV(ball.pos, ball.radio, ball.color);
	}
	void DrawPlayers(PLAYER pj1, PLAYER pj2)
	{
		DrawRectangleRec(pj1.pos, pj1.color);
		DrawRectangleRec(pj2.pos, pj2.color);
	}
	void DrawTimer(int timer)
	{
		if (timer > 60)
		{
			if (timer > 120)
			{
				ClearBackground(RAYWHITE);
				DrawText(FormatText("3"), middlescreenWidth, middlescreenHeight, 20, DARKGRAY);
			}
			else
			{
				DrawText(FormatText("2"), middlescreenWidth, middlescreenHeight, 20, DARKGRAY);
			}
			
		}
		else
		{
			DrawText(FormatText("1"), middlescreenWidth, middlescreenHeight, 20, DARKGRAY);
		}
		
	}
	void DrawObstacles(time::POWERxTIME pxt)
	{
		if (pxt.obstB)
		{
			DrawRectangleRec(pxt.obst.rec1, BLACK);
			DrawRectangleRec(pxt.obst.rec2, BLACK);
		}
	}
	void DrawShield(PLAYER pj1, PLAYER pj2)
	{
		if (pj1.shield.shieldBool)
		{
			DrawRectangleRec(pj1.shield.shield, pj1.color);
		}
		if (pj2.shield.shieldBool)
		{
			DrawRectangleRec(pj2.shield.shield, pj2.color);
		}
	}
	void DrawBall2(BALL ball2)
	{
		if (ball2.existo)
		{
			DrawCircleV(ball2.pos, ball2.radio, ball2.color);
		}
	}
	void DrawPower(entity::POWER pwr)
	{
		if (pwr.exist)
		{
			DrawCircle(pwr.pos.x, pwr.pos.y, pwr.radio, pwr.color);
		}
	}
}
