#ifndef DRAW_H
#define DRAW_H

#include "raylib.h"

#include "Entities/Screen.h"
#include "Entities/Player.h"
#include "Entities/ball.h"
#include "Entities/PowerUps.h"

#include "ENUMS/Winers.h"

#include "Others/Size Letters.h"

namespace draw_menus
{
	void DrawIntro();

	void DrawGeneral();

	void DrawConfigPads(PLAYER& pj1, PLAYER& pj2, bool bot);

	void DrawOpciones(PLAYER pj, PLAYER pj2);

	void DrawCredits();
}
namespace draw_game
{
	void DrawGame(BALL ball, PLAYER pj1, PLAYER pj2, powers::time::POWERxTIME pxt, BALL ball2, int timer, powers::entity::POWER pwr);

	void DrawPause();

	void DrawEnd(PLAYERS w);
}

#endif