#include "game.h"

namespace pong 
{
	namespace game 
	{
		const int screenWidth = 800;
		const int screenHeight = 450;
		
		Scene currentScene;


		void run()
		{
			init();

			while (!WindowShouldClose())
			{
				update();
				draw();
			}

			deinit();
		}

		static void init()
		{
			InitWindow(screenWidth, screenHeight, "PONG");
			SetTargetFPS(60);
			menu::init();
			gameplay::init();
			SetExitKey(999);

		}

		static void update()
		{
			switch (currentScene)
			{
			case Scene::Menu:
				menu::update();
				break;
			case Scene::Game:
				gameplay::update();
				break;
			default:
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();
			ClearBackground(RAYWHITE);

			switch (currentScene)
			{
			case Scene::Menu: 
				menu::draw();
				break;
			case Scene::Game: 
				gameplay::draw();
				break;
			}

			EndDrawing();
		}

		static void deinit()
		{
			CloseWindow();

			menu::deinit();
			gameplay::deinit();
		}
	}
}