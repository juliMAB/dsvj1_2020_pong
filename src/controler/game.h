#ifndef GAME_H
#define GAME_H

#include "raylib.h"

#include "Scenes/menu.h"
#include "Scenes/gameplay.h"

namespace pong
{
	namespace game
	{
		extern const int screenWidth;
		extern const int screenHeight;

		enum class Scene
		{
			Menu,
			Game
		};	

		extern Scene currentScene;

		void run();

		static void init();
		static void update();
		static void draw();
		static void deinit();
	}
}

#endif